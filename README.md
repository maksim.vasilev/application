# Сайт robocraftlab.ru
## запуск проекта
 1. создать кластер используя [terraform](https://gitlab.com/otus-final/terraform)
  - деплой из ветки development создает(обновляет) кластер development окружения 
  - деплой из ветки master создает(обновляет) кластер для production окружения
 2. запустить pipeline [application](https://gitlab.com/otus-final/application/pipelines)
  - шаги:
    - build image, в зависимости от branch development || master собираются разные образы и происходит push в docker hub
    - get kubeconfig, используя gcp service account подключается к нашему кластеру, и используя webhook gitlab сохраняет(обновляет) конфиг в секретах для дальнейшей работы по deploy
    - используя полученный kube-config сравниваем в кластере окружения helm-chart(до и после)
    - используя полученный kube-config деплоим обновленные helm-chart в дев или мастер(в зависимости от branch)
    - push_secrets_restore_db: 
      - обновляем секреты и деплоим, в зависимости от branch
      - деплоим(обновляем) базу данных postgres, создам PVC, Если база пустая накатываем dump, если нет, попускаем этот шаг.
    - deploy_application, в зависимости от окружения(branch) деплоим изменения deployment,service,ingress ruls, в наши кластера. в конце pipeline получаем наш ip url на который можно зайти.
## используемый framework gjango, задействованые сущности:
 - db layer
   - sql - postgres
   - nosql - redis
 - application layer
   - uwsgi
   - web-server-django
   - celery async worker
   - celery flower - create task for async jobs 
   - celery beat - cron job for run celery async tasks
- monitoring
  - helm-chart GAP
  - prometheus exporter nginx
  - prometheus exporter postgres
  - Для получения статистики nginx+uwsgi необходимо добавить dashboard grafana
   - import grafana dashboard devops/grafana-dashboard/nginx-dashboard
  - для получения статистики postgres необходимо добавить dashboard grafana
   - import grafana dashboard devops/grafana-dashboard/{Postgres Overview.json, postgres-Statistics.json}
- TLS
  - helm-chart cert-manager
- ingress == nginx helm-chart
- logging 
  - helm-chart grafana loki & promtail
  - для просмотра логов приложения нужно испортировать grafana dashboard devops/grafana-dashboards/nginx-ingress
- проверка работы приложения:
 - на последнем шаге выполнения ci/cd будут доступные URL приложения
```
    - echo "visit site https://$new_external_url"
    - echo "visit admin site https://$new_external_url/_admin_r0b0"
    - echo "visit monitoring site https://grafana.$new_external_url"
    - echo "visit flower monitoring http://flower.$new_external_url"
    - echo "visit promitheus monitoring http://prometheus.$new_external_url"
```

