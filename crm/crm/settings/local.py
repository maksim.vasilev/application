import socket

from .base import *

ALLOWED_HOSTS = ['*']

INSTALLED_APPS += (
    'debug_toolbar',
)

ip = socket.gethostbyname(socket.gethostname())

INTERNAL_IPS = (
    '127.0.0.1',
    ip[:-1] + '1'
)


MIDDLEWARE += [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

SHOW_DEBUG_TOOLBAR = False


def show_toolbar(request):
    return SHOW_DEBUG_TOOLBAR


DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': show_toolbar,
}
