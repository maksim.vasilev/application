from .base import *

DEBUG = False

ALLOWED_HOSTS = [
    '*'
]


MEDIA_ROOT = os.path.join(BASE_DIR, "/robocraft/files/media/")
STATIC_ROOT = os.path.join(BASE_DIR, "/robocraft/files/static/")

ACCOUNT_DEFAULT_HTTP_PROTOCOL = 'http' if DEBUG else 'https'
