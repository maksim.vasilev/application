from django.test import TestCase
from django.utils import timezone
from core.models import User
from organization.models import Organization
from tariff.models import SeasonTicket, Tariff, Course, Section


class TestSeasonTicket(TestCase):

    def test_create(self):
        buyer_user = User.objects.create(username='user_1')
        student_user = User.objects.create(username='child_1')
        org = Organization.objects.create(name='ИП Дубовицкий')
        section = Section.objects.create(name='section', organization=org,
                                         sorting_number=1)
        course = Course.objects.create(section=section, name='Тестовый курс',
                                       sorting_number=1)
        tariff = Tariff.objects.create(
            course=course, name='тестовый тариф 1',
            cost=2000, lesson_count=5, expiration_date=30)
        ticket = SeasonTicket.objects.create(
            tariff=tariff,
            expiration_date=timezone.now(), student=student_user)
        code = ticket.code
        self.assertEqual(len(code), 10)

        ticket.buyer = buyer_user
        ticket.save()
        self.assertEqual(code, ticket.code)
