from django.urls import reverse_lazy
from django.test import TestCase

from blog.models import Article
from core.models import User


class TestSmoke(TestCase):

    def test_blog_empty_list(self):

        for code, name in Article.ARTICLE_TYPE_CHOICES:
            url = reverse_lazy('blog:list', kwargs={'article_type': code})
            resp = self.client.get(url)
            self.assertEqual(resp.status_code, 200)
            self.assertEqual(len(resp.context_data['article_list']), 0)

    def test_blog_list_detail(self):
        news_1 = Article.objects.create(article_type=Article.NEWS,
                                        title='title-1', slug='title-1')
        article_1 = Article.objects.create(article_type=Article.ARTICLES,
                                           title='title-2', slug='title-2')

        for type_, name in Article.ARTICLE_TYPE_CHOICES:
            url = reverse_lazy('blog:list', kwargs={'article_type': type_})
            resp = self.client.get(url)
            self.assertEqual(resp.status_code, 200)
            self.assertEqual(len(resp.context_data['article_list']), 1)

    def test_sitemap(self):
        resp = self.client.get(
            reverse_lazy('django.contrib.sitemaps.views.sitemap'))
        self.assertEqual(resp.status_code, 200)

    def test_timetable(self):
        user = User.objects.create(username='user')
        self.client.force_login(user)

        resp = self.client.get(
            reverse_lazy('timetable:current_month'))
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get(
            reverse_lazy('timetable:month',
                         kwargs={'year': 2019, 'month': 12}))
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get(
            reverse_lazy('timetable:current_week'))
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get(
            reverse_lazy('timetable:week',
                         kwargs={'year': 2019, 'month': 12, 'day': 31}))
        self.assertEqual(resp.status_code, 200)

