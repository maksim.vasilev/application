from django.core.exceptions import ValidationError
from django.test import TestCase

from core.models import User
from organization.models import Organization
from lesson.models import Lesson, StudentOnLesson
from tariff.models import Course, Tariff, SeasonTicket


class TestLesson(TestCase):

    def test_flow(self):
        organization = Organization.objects.create(
            name='Организация 1'
        )

        course_robo_1 = Course.objects.create(
            name='Первый уровень робототехника', sorting_number=1,
            organization=organization
        )
        course_robo_2 = Course.objects.create(
            name='Второй уровень робототехника', sorting_number=2,
            organization=organization
        )

        course_eng_1 = Course.objects.create(
            name='Первый уровень английский', sorting_number=3,
            organization=organization
        )

        tariff_5_1_robo = Tariff.objects.create(
            name='5 занятий', course=course_robo_1, lesson_count=5,
            cost=2500, expiration_date=90
        )

        tariff_12_2_robo = Tariff.objects.create(
            name='12 занятий', course=course_robo_2, lesson_count=12,
            cost=5000, expiration_date=90
        )

        tariff_5_eng = Tariff.objects.create(
            name='5 занятий', course=course_eng_1, lesson_count=5,
            cost=3000, expiration_date=90
        )

        buyer_1 = User.objects.create(username='buyer_1')
        buyer_2 = User.objects.create(username='buyer_2')

        student_1 = User.objects.create(username='student_1')
        student_2 = User.objects.create(username='student_2')
        student_3 = User.objects.create(username='student_3')

        season_ticket_1 = SeasonTicket.objects.create(
            buyer=buyer_1, student=student_1, tariff=tariff_5_1_robo
        )
        season_ticket_2 = SeasonTicket.objects.create(
            student=student_2, tariff=tariff_12_2_robo,
            discount=8
        )

        lesson_1 = Lesson.objects.create(
            course=course_robo_1, name='Wedo 1',
            date='2019-09-09', time_start='14:00', time_end='16:00'
        )

        # курс абонемента не совпадает с курсом урока
        self.assertRaises(
            ValidationError, StudentOnLesson.objects.create,
            was_away=False, lesson=lesson_1, season_ticket=season_ticket_2
        )

        StudentOnLesson.objects.create(
            was_away=False, lesson=lesson_1, season_ticket=season_ticket_1
        )
        self.assertEqual(season_ticket_1.lesson_balance, 4)

