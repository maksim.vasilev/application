from django.conf.urls import include
from django.urls import path
from .views import HealthCheck, TochkaCallback, RunSomeTask

# app_name = 'api'

urlpatterns = [
    path('health/', HealthCheck.as_view(), name='health'),
    path('v1/banks/tochka/callback/', TochkaCallback.as_view(),
         name='tochka_callback'),
    path('v1/run_some_task/', RunSomeTask.as_view(),
         name='run_some_task'),
]
