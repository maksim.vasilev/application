from rest_framework.views import APIView
from rest_framework.response import Response

from crm.tasks import some_task


class HealthCheck(APIView):

    def get(self, request):
        return Response({'health': 'OK'})


class TochkaCallback(APIView):

    def post(self, request):
        return Response({})


class RunSomeTask(APIView):

    def post(self, request):
        some_task.delay(**request.data)
        return Response({})
