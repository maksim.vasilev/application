from django.urls import path
from . import views

app_name = 'private'

urlpatterns = [
    path('', views.PrivateIndexView.as_view(), name='index'),
    path('season_ticket/list', views.PrivateSeasonTicketListView.as_view(),
         name='season_ticket_list'),
    path('season_ticket/paid/<season_ticket_pk>',
         views.SeasonTicketPaidView.as_view(),
         name='season_ticket_paid'),

    path('lessons', views.PrivateLessonListView.as_view(),
         name='lesson_list'),
    path('lessons/detail/<lesson_pk>', views.PrivateLessonDetailView.as_view(),
         name='lesson_detail'),
]
