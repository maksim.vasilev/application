import datetime
from django.core.exceptions import PermissionDenied
from django.db.models import Sum
from django.views.generic import TemplateView, ListView, DetailView, UpdateView
from organization.models import Organization
from lesson.models import Lesson, PhotoFromLesson
from payment.models import Payment
from django.utils.timezone import now, localtime


from .forms import PhotoFromLessonForm, LessonListFilterForm


def check_user_is_org_instructor(request):
    org = Organization.objects.first()
    if request.user.is_authenticated and \
            request.user.instructor_set.filter(organization=org).exists():
        return True
    return False


class IndexView(TemplateView):
    template_name = 'dashboard/index.html'


class CheckUserDashboardPermissionMixin:
    def dispatch(self, request, *args, **kwargs):
        if not check_user_is_org_instructor(request):
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)


class LessonListView(CheckUserDashboardPermissionMixin, ListView):
    template_name = 'dashboard/lesson_list.html'
    model = Lesson
    paginate_by = 100
    queryset = (Lesson.objects
                .all()
                .prefetch_related('studentonlesson_set__season_ticket__student'))  # noqa

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter_form'] = LessonListFilterForm()
        return context


class LessonDetailView(CheckUserDashboardPermissionMixin, UpdateView):
    template_name = 'dashboard/lesson_detail.html'
    model = Lesson
    fields = ('name', )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['photo_form'] = PhotoFromLessonForm
        return context

    def post(self, request, *args, **kwargs):
        form = self.get_form(PhotoFromLessonForm)
        files = request.FILES.getlist('photos')
        if form.is_valid():
            new_photos = []
            for f in files:
                new_photos.append(
                    PhotoFromLesson(photo=f, lesson=self.get_object())
                )
            PhotoFromLesson.objects.bulk_create(new_photos)
        return super().post(request, *args, **kwargs)


class PaymentStatView(TemplateView):
    template_name = 'dashboard/payments.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        payments = Payment.objects.filter(status=Payment.PAID)
        dt_now = localtime(now())
        context['current_month_sum'] = (
            payments
            .filter(created__month=dt_now.month, created__year=dt_now.year)
            .aggregate(Sum('amount'))
        )
        context['prev_month_sum'] = (
            payments
            .filter(created__month=dt_now.month-1, created__year=dt_now.year)
            .aggregate(Sum('amount'))
        )
        return context
