import datetime
import functools

from ckeditor.fields import RichTextField

from django.db import models
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.utils.timezone import now, localtime

from tariff.models import Course, SeasonTicket
from organization.models import Instructor
from utils.utils import get_upload_file_path


User = get_user_model()

get_lesson_image_path = functools.partial(get_upload_file_path,
                                          folder='lesson_photo')


class LessonContext(models.Model):
    name = models.CharField('Название урока', max_length=120)
    context = RichTextField('Конспект урока', blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Конспект урока'
        verbose_name_plural = 'Конспекты уроков'


class Lesson(models.Model):
    course = models.ForeignKey(Course, on_delete=models.CASCADE,
                               verbose_name='Курс')
    lesson_context = models.ForeignKey(
        LessonContext, on_delete=models.SET_NULL, verbose_name='Конспект урока',
        blank=True, null=True)
    instructor = models.ForeignKey(
        Instructor, on_delete=models.SET_NULL, verbose_name='Преподаватель',
        null=True)
    name = models.CharField('Назание урока', max_length=255)
    date = models.DateField('Дата')
    description = RichTextField('Описание', blank=True)
    time_start = models.TimeField('Время начала')
    time_end = models.TimeField('Время завершения')

    def __str__(self):
        return f'{self.name} | {self.date}'

    class Meta:
        verbose_name = 'Занятие'
        verbose_name_plural = 'Занятия'
        ordering = ('-date', '-time_start')

    @property
    def is_completed(self):
        dt_now = localtime(now())
        return str(self.end_datetime) <= str(dt_now)

    @property
    def student_count(self):
        return self.studentonlesson_set.count()

    @property
    def end_datetime(self):
        return datetime.datetime.combine(self.date, self.time_end)

    @property
    def students_names(self):
        return ', '.join([
            s.season_ticket.student.shot_name for s in
            self.studentonlesson_set.all() if
            (s.season_ticket and s.season_ticket.student)
        ])
    students_names.fget.short_description = 'Имена обучающихся'


class StudentOnLesson(models.Model):
    lesson = models.ForeignKey(
        Lesson, on_delete=models.CASCADE, verbose_name='Урок')
    season_ticket = models.ForeignKey(
        SeasonTicket, on_delete=models.CASCADE,
        verbose_name='Абонемент обучающегося',
        null=True)
    comment = models.CharField('Комментарий', max_length=200, blank=True)
    was_away = models.BooleanField('Отсутствовал', blank=True)

    def __str__(self):
        return f'{self.season_ticket}'

    class Meta:
        verbose_name = 'Обучающийся на уроке'
        verbose_name_plural = 'Обучающиеся на уроке'
        unique_together = (('lesson', 'season_ticket'),)

    def clean(self):
        if self.season_ticket and hasattr(self.lesson, 'course') and \
                self.lesson.course != self.season_ticket.tariff.course:
            raise ValidationError({
                'season_ticket': 'Курс урока не совпадает с курсом абонемета'})

        if self not in self.season_ticket.studentonlesson_set.all() and \
                self.season_ticket.lesson_balance <= 0:
            raise ValidationError({
                'season_ticket': 'Баланс уроков на абонементе закончился'
            })
        super(StudentOnLesson, self).clean()

    def save(self, *args, **kwargs):
        self.full_clean()
        super(StudentOnLesson, self).save(*args, **kwargs)


class PhotoFromLesson(models.Model):
    lesson = models.ForeignKey(
        Lesson, on_delete=models.CASCADE, verbose_name='Урок')
    photo = models.ImageField('Фото', upload_to=get_lesson_image_path)
    comment = models.CharField('Комментарий', max_length=255, blank=True)

    def __str__(self):
        return self.lesson.name

    class Meta:
        verbose_name = 'Фотография с урока'
        verbose_name_plural = 'Фотографии с урока'
