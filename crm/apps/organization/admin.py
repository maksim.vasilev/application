from django.contrib import admin

from organization.models import Organization, Instructor


class OrganizationAdmin(admin.ModelAdmin):
    search_fields = ('name', 'domain', 'managers')
    list_fields = ('name', 'slug', 'managers')
    prepopulated_fields = {'domain': ('managers', )}
    raw_id_fields = ('managers',)


admin.site.register(Organization, OrganizationAdmin)
admin.site.register(Instructor)
