from django.urls import path
from . import views

app_name = 'organization'

urlpatterns = [
    path('prepodavateli/', views.InstructorListView.as_view(),
         name='instructor_list'),
]
