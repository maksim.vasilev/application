import calendar

from django import template

register = template.Library()


@register.simple_tag
def get_month_name(year: int, month: int, next_: bool = False,
                   prev: bool = False) -> (str, int):
    if not month:
        return
    if next_:
        year, month = calendar.nextmonth(year, month)
    if prev:
        year, month = calendar.prevmonth(year, month)
    return year, month, calendar.month_name[month]

