import datetime
import calendar
import locale

from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from django.utils.decorators import method_decorator
from django.utils.timezone import now, localtime


now = localtime(now())

# локаль для календаря
locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')


class MonthCalendarWithoutZeroDays(calendar.LocaleHTMLCalendar):

    def __init__(self, year: int, month: int, firstweekday: int = 0,
                 locale_: str = None):
        calendar.LocaleHTMLCalendar.__init__(self, firstweekday, locale_)
        self.year = year
        self.month = month

    def formatmonth_without_zero_days(self, withyear=True):
        data = self.formatmonth(self.year, self.month, withyear)
        return data

    def monthdays2calendar(self, year, month):
        return self.monthdatescalendar(year, month)

    def formatday_without_zero(self, year, month, day, weekday):
        class_ = self.cssclasses[weekday]
        if month != self.month:
            class_ += ' text-muted'
        return f'<td class="{class_}" ' \
            f'month="{month}" year="{year}">{day}</td>'

    def formatweek(self, theweek):
        """
        Return a complete week as a table row.
        """
        s = ''.join(self.formatday_without_zero(dd.year, dd.month, dd.day, d)
                    for d, dd in enumerate(theweek))
        return '<tr class="week">%s</tr>' % s


class TimeTableMonthView(TemplateView):
    template_name = 'timetable/timetable_month.html'

    @method_decorator(login_required)
    def get(self, request, year: int = now.year, month: int = now.month,
            *args, **kwargs):
        context = self.get_context_data(**kwargs)
        cl = MonthCalendarWithoutZeroDays(year, month)
        context['calendar_html'] = cl.formatmonth_without_zero_days()
        context['year'] = year
        context['month'] = month
        return self.render_to_response(context)


class TimeTableWeekView(TemplateView):
    template_name = 'timetable/timetable_week.html'

    @method_decorator(login_required)
    def get(self, request, year: int = now.year, month: int = now.month,
            day: int = None, *args, **kwargs):
        context = self.get_context_data(**kwargs)

        if not day:
            day = datetime.datetime.today() - \
                  datetime.timedelta(
                      days=datetime.datetime.today().weekday() % 7)
            day = day.day

        context['week_days'] = [
            datetime.date(year, month, day) + datetime.timedelta(days=i)
            for i in range(7)
        ]

        return self.render_to_response(context)
