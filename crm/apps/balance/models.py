from django.db import models
from django.db.models import Q

from core.models import User
from tariff.models import Course, SeasonTicket


class StudentLessonBalance(models.Model):
    student = models.ForeignKey(User, on_delete=models.CASCADE,
                                verbose_name='Обучающийся')
    course = models.ForeignKey(Course, on_delete=models.CASCADE,
                               verbose_name='Курс')
    lesson_balance = models.PositiveIntegerField('Остаток уроков', default=0)

    class Meta:
        verbose_name = 'Баланс уроков'
        verbose_name_plural = 'Балансы уроков'
