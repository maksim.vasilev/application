import os
import uuid


def get_upload_file_path(instance, filename: str, folder: str) -> str:
    ext = filename.split('.')[-1]
    id_ = instance.lesson.id if hasattr(instance, 'lesson') else instance.id
    filename = f'{id_}-{uuid.uuid4()}.{ext}'
    return os.path.join(f'uploads/{folder}', filename)
