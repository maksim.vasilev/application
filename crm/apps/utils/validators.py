import phonenumbers
from phonenumbers.phonenumberutil import NumberParseException

from django.core.exceptions import ValidationError


def validate_phone_number(value: str):
    try:
        phone = phonenumbers.parse(value, 'RU')
        return phonenumbers.is_valid_number_for_region(phone, 'RU')
    except NumberParseException:
        pass

    raise ValidationError('Неверный формат номера телефона')
