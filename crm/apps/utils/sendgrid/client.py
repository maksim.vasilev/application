import os
import logging

from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

logger = logging.getLogger('crm')

sendgrid_client = SendGridAPIClient(os.getenv('SENDGRID_API_KEY'))


def send_message(from_email: str, to_emails: list, subject: str,
                 plain_text_content: str):
    message = Mail(
        from_email=from_email,
        to_emails=to_emails,
        subject=subject,
        plain_text_content=plain_text_content)
    try:
        logger.info(f'Send email: {message}')
        resp = sendgrid_client.send(message)
        logger.info(f'Send email result: {resp.status_code}, '
                    f'dict: {resp.to_dict}')
    except Exception as e:
        logger.error(f'Email send error {e}')

