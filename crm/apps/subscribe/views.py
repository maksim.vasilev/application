from django.views import View
from django.views.generic.edit import FormView, CreateView
from django.shortcuts import render
from django.urls import reverse_lazy

from .forms import SubscribeForm
from .models import Subscribe


class SubscribeGetCreateView(CreateView):
    model = Subscribe
    form_class = SubscribeForm
    template_name = 'subscribe/create.html'
    success_url = reverse_lazy('subscribe:success')

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {'form': self.form_class})


class SuccessView(View):
    template_name = 'subscribe/success.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)
