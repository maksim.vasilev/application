from django.urls import path
from . import views

app_name = 'subscribe'

urlpatterns = [
    path('new/', views.SubscribeGetCreateView.as_view(), name='new'),
    path('success/', views.SuccessView.as_view(), name='success'),
]
