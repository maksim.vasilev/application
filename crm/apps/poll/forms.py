from typing import Type

from django import forms

from utils.validators import validate_phone_number

from .models import Question, Poll


POLL_FIELD_PREFIX = 'question-'


def poll_factory_form(poll: Poll) -> Type[forms.Form]:

    fields = {}

    for question in poll.question_set.all().order_by('sort_num'):
        init_kwargs = {
            'label': question.question_text + ':',
            'required': question.is_required
        }
        field_name = f'{POLL_FIELD_PREFIX}{question.id}'

        if question.field_type == Question.CHOICE:
            field = forms.ChoiceField
            init_kwargs.update({
                'choices': [
                    (val, val) for val in question.field_data.split(',')
                ] if question.field_data else None
            })

        elif question.field_type == Question.TEXT:
            field = forms.CharField
            init_kwargs.update({
                'widget': forms.Textarea
            })

        elif question.field_type == Question.PHONE:
            field = forms.CharField
            init_kwargs.update({
                'validators': [validate_phone_number]
            })

        elif question.field_type == Question.INTEGER:
            field = forms.IntegerField

        else:
            field = forms.CharField

        if not field:
            raise ValueError

        fields[field_name] = field(**init_kwargs)

    form_class: Type[forms.Form] = type('QuestionForm', (forms.Form,), fields)
    return form_class
