from django.db import models

from organization.models import Organization


class Poll(models.Model):
    organization = models.ForeignKey(
        Organization, verbose_name='Организация', on_delete=models.SET_NULL,
        null=True, blank=True)
    title = models.CharField(max_length=100, verbose_name='Заголовок')
    slug = models.SlugField()
    post_button_text = models.CharField(
        default='Отправить', max_length=50,
        verbose_name='Текст на кнопке отправки')
    is_active = models.BooleanField(verbose_name='Активен', default=True)
    email = models.EmailField('Отправлять ответы на email', blank=True,
                              null=True)
    created = models.DateTimeField(auto_now_add=True,
                                   verbose_name='Дата создания')

    def __str__(self):
        return self.title

    @property
    def poll_name(self):
        return f'{self.organization}-{self.title}'

    class Meta:
        verbose_name = 'Опрос'
        verbose_name_plural = 'Опросы'

    def get_post_num(self):
        required_question = Question.objects.\
            filter(is_required=True, poll=self).first()
        posts = Answer.objects.\
            filter(question=required_question).count()
        return posts


class Question(models.Model):

    TEXT = 'text'
    STRING = 'char'
    CHOICE = 'choice'
    INTEGER = 'integer'
    PHONE = 'phone'

    FIELD_TYPE_CHOICES = (
        (STRING, 'Строка'),
        (TEXT, 'Текст'),
        (CHOICE, 'Один из списка'),
        (INTEGER, 'Число'),
        (PHONE, 'Номер телефона'),
    )

    poll = models.ForeignKey(Poll, on_delete=models.CASCADE)
    question_text = models.CharField(max_length=255,
                                     verbose_name='Текст вопроса')
    field_type = models.CharField(max_length=20, verbose_name='Тип поля ввода',
                                  choices=FIELD_TYPE_CHOICES,
                                  default=STRING)
    field_data = models.CharField(max_length=255, blank=True, null=True,
                                  verbose_name='Данные поля')
    is_unique = models.BooleanField(verbose_name='Уникальный ответ',
                                    default=False)
    is_required = models.BooleanField(verbose_name='Обязательный ответ',
                                      default=True)

    sort_num = models.PositiveIntegerField(
        verbose_name='Порядок',
        help_text='Порядок расположения полей формы (сверху вниз)',
        default=1)

    def __str__(self):
        return self.question_text

    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы'
        ordering = ('poll', 'sort_num')


class Answer(models.Model):
    session = models.CharField(max_length=32, verbose_name='Сессия')
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    answer_text = models.CharField(max_length=255, verbose_name='Текст ответа')
    created = models.DateTimeField(auto_now_add=True,
                                   verbose_name='Дата создания')

    def __str__(self):
        return self.answer_text

    class Meta:
        verbose_name = 'Ответ'
        verbose_name_plural = 'Ответы'
