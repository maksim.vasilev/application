from django.urls import path
from . import views

app_name = 'poll'

urlpatterns = [
    path('<slug:slug>/', views.PollView.as_view(), name='detail'),
    path('', views.PollListView.as_view(), name='list'),
]
