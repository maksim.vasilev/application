from django.contrib import admin

from .models import Poll, Question, Answer


class QuestionInline(admin.TabularInline):
    model = Question
    extra = 0


class PollAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'is_active',)
    list_filter = ('is_active',)
    search_fields = ('title',)
    inlines = [QuestionInline]
    prepopulated_fields = {'slug': ('title', 'organization')}


class AnswerAdmin(admin.ModelAdmin):
    list_display = ('question', 'answer_text', 'session', 'created')
    search_fields = ('answer_text', 'session')
    list_filter = ('question__poll', 'question', 'created',)


admin.site.register(Poll, PollAdmin)
admin.site.register(Question)
admin.site.register(Answer, AnswerAdmin)
