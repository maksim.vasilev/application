from django import forms

from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin, UserCreationForm

from tariff.models import SeasonTicket

User = get_user_model()


class SeasonTicketBuyerInline(admin.TabularInline):
    model = SeasonTicket
    extra = 1
    fk_name = 'buyer'
    verbose_name = 'Купленный абонемент'
    verbose_name_plural = 'Купленные абонементы'
    exclude = ('seller', 'expiration_date', 'comment')
    raw_id_fields = ('student',)


class SeasonTicketStudentInline(SeasonTicketBuyerInline):
    fk_name = 'student'
    verbose_name = 'Абонемент обучаемого'
    verbose_name_plural = 'Абонементы обучаемого'
    raw_id_fields = ('buyer',)


class CustomUserCreationForm(UserCreationForm):
    """
    A UserCreationForm with optional password inputs.
    """

    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        self.fields['password1'].required = False
        self.fields['password2'].required = False
        # If one field gets autocompleted but not the other, our 'neither
        # password or both password' validation will be triggered.
        self.fields['password1'].widget.attrs['autocomplete'] = 'off'
        self.fields['password2'].widget.attrs['autocomplete'] = 'off'

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = super(CustomUserCreationForm, self).clean_password2()
        if bool(password1) ^ bool(password2):
            raise forms.ValidationError("Fill out both fields")
        return password2


class CustomUserAdmin(UserAdmin):
    ordering = ('-id',)
    model = User
    add_form = CustomUserCreationForm

    list_display = ('id', 'first_name', 'last_name', 'second_name', 'username',
                    'phone', 'is_student', 'age')

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('is_student', 'last_name', 'first_name', 'second_name',
                       'phone', 'email',  'username', 'is_active'
                       # 'password1', 'password2',
                       )
        }),

    )
    search_fields = ('phone', 'email', 'username', 'first_name', 'last_name',
                     'comment')
    list_filter = ('is_student', 'is_superuser', )

    prepopulated_fields = {'username': ('last_name', 'first_name',
                                        'second_name')}
    inlines = (SeasonTicketBuyerInline, SeasonTicketStudentInline,)
    fieldsets = (
        (None, {
            'fields':
                ('photo', 'is_student',
                 'last_name', 'first_name', 'second_name', 'phone', 'email',
                 'birthday', 'comment', 'username', 'last_login',
                 'date_joined')}),

        ('Права', {'fields': (
            'is_active', 'is_staff', 'is_superuser',
            # 'groups', 'user_permissions'
        ),
        }),
    )

    def get_full_name(self, obj):
        return obj.get_full_name()


admin.site.register(User, CustomUserAdmin)
