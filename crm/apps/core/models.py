import logging
import functools
from datetime import date
from django.contrib.auth.models import AbstractUser, Group
from django.core.exceptions import ValidationError
from django.db import models

from utils.validators import validate_phone_number
from utils.utils import get_upload_file_path


logger = logging.getLogger(__name__)


get_user_image_path = functools.partial(get_upload_file_path,
                                        folder='user_photo')


class User(AbstractUser):
    photo = models.ImageField('Фото', blank=True, null=True,
                              upload_to=get_user_image_path)
    phone = models.CharField(
        'Номер телефона', max_length=10, blank=True, null=True,
        validators=[validate_phone_number])
    second_name = models.CharField('Отчество', max_length=100, blank=True)
    birthday = models.DateField('Дата рождения', blank=True, null=True)
    is_student = models.BooleanField('Обучаемый', blank=True, default=False)
    comment = models.TextField('Комментарий', blank=True, null=True)
    updated = models.DateTimeField('Дата изменения', auto_now=True)

    def __str__(self):
        return self.get_full_name()

    def clean(self):
        user = User.objects.filter(email=self.email).first()
        if self.email and user and user != self:
            raise ValidationError({
                'email': 'Пользователь с таким email уже существует'})

        user = User.objects.filter(phone=self.phone).first()
        if self.phone and user and user != self:
            raise ValidationError({
                'phone': 'Пользователь с таким номером телефона '
                         'уже существует'})

    def get_full_name(self):
        full = f'{self.first_name} {self.second_name} {self.last_name}'
        return full.strip() if len(full.replace(' ', '')) else self.username

    @property
    def shot_name(self):
        return f'{self.first_name} {self.last_name}'

    @property
    def age(self):
        if self.birthday:
            today = date.today()
            return today.year - self.birthday.year - (
                    (today.month, today.day) <
                    (self.birthday.month, self.birthday.day)
            )
