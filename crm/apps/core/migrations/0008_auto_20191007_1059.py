# Generated by Django 2.2.5 on 2019-10-07 10:59

from django.db import migrations, models
import functools
import utils.utils


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_find_users'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='photo',
            field=models.ImageField(blank=True, null=True, upload_to=functools.partial(utils.utils.get_upload_file_path, *(), **{'folder': 'user_photo'}), verbose_name='Фото'),
        ),
    ]
