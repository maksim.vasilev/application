import logging

from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from allauth.account.utils import perform_login
from .models import User


logger = logging.getLogger('crm')


class SocialAccountAdapter(DefaultSocialAccountAdapter):
    def pre_social_login(self, request, sociallogin):
        user = sociallogin.user
        if user.id:
            return
        try:
            # if user exists, connect the account
            # to the existing account and login
            if user.email:
                customer = User.objects.get(email=user.email)
                sociallogin.state['process'] = 'connect'
                perform_login(request, customer, 'none')
        except Exception as e:
            logger.error(f'Social account pre_login error: {e}')
