from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.http import Http404
from django.urls import reverse_lazy

from organization.models import Organization
from tariff.models import SeasonTicket

from payment.models import Payment
from bank.sberbank import SberBankClient


class PaidSeasonTicketMixin:

    @staticmethod
    def _get_success_url(request):
        return settings.ACCOUNT_DEFAULT_HTTP_PROTOCOL + '://' + \
               request.META['HTTP_HOST'] + str(reverse_lazy('payment:success'))

    @staticmethod
    def get_client():
        org = Organization.objects.first()
        sberbank_client = SberBankClient(
            org.sb_url_api, org.sb_username_api, org.sb_password_api)
        return sberbank_client

    def paid_season_ticket(self, request, season_ticket: SeasonTicket,
                           comment: str = None) -> (dict, bool):

        sb_client = self.get_client()

        payment = Payment.objects.create(
            organization=Organization.objects.first(),
            user=request.user,
            content_type=ContentType.objects.get_for_model(SeasonTicket),
            amount=season_ticket.debt,
            object_id=season_ticket.id,
            comment=comment,
            payment_type=Payment.SB
        )

        order_number = payment.id if not settings.DEBUG \
            else f'debug-{payment.id}'

        resp, error = sb_client.do_register(
            request.user.id,
            order_number,
            int(season_ticket.debt * 100),  # в копейках
            self._get_success_url(request)
        )

        if error:
            payment.status = Payment.ERROR
        else:
            payment.acquiring_order_id = resp.get('orderId')
            payment.status = Payment.PENDING

        payment.info = resp
        payment.save()

        return resp, error

    def get_paid_result(self, acquiring_order_id: str) -> (dict, bool):
        payment = Payment.objects.filter(
            acquiring_order_id=acquiring_order_id).last()

        if not payment:
            raise Http404('payment not found')

        sb_client = self.get_client()

        resp, error = {}, False

        if payment.status != Payment.PAID:
            resp, error = sb_client.get_order_status(
                order_id=acquiring_order_id, order_number=payment.id)

            payment.info = resp
            payment.status = Payment.PAID if resp['orderStatus'] == 2 \
                else Payment.ERROR
            payment.save()

        return resp, error
