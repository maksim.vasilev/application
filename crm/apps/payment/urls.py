from django.urls import path
from . import views

app_name = 'payment'

urlpatterns = [
    path('success/', views.PaymentSuccess.as_view(),
         name='success'),
]
