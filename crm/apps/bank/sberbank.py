import logging
import requests

from django.conf import settings


logger = logging.getLogger('crm')


class SberBankClient:

    HEADERS = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }

    def __init__(self, url: str, username: str, password: str):
        self.url = url
        self.username = username
        self.password = password

    def _request(self, request_name: str, data: dict) -> (dict, bool):
        url = self.url + request_name
        data.update(
            userName=self.username,
            password=self.password,
        )
        logger.info(f'SberBank request url: {url}, with data: {data}')
        resp = requests.post(url, data=data, headers=self.HEADERS)
        json = resp.json()
        logger.info(f'Sberbank request response with status: {resp}, '
                    f'data: {json}')

        if resp.status_code != 200 or (
                json.get('errorCode') != '0'
                if json.get('errorCode') else False):
            logger.error(f'Sberbank error: {json}')
            return json, True

        return json, False

    def do_register(self, client_id: int, order_number: str, amount: int,
                    return_url: str = '/', json_params: dict = None):
        request_name = 'register.do'
        data = {
            'orderNumber': order_number,
            'amount': amount,
            'clientId': client_id,
            'returnUrl': return_url
        }

        if json_params:
            data.update(json_params)

        return self._request(request_name, data=data)

    def get_order_status(self, order_id: str, order_number: str):
        request_name = 'getOrderStatusExtended.do'
        data = {
            'orderId': order_id,
            'orderNumber': order_number,
        }
        return self._request(request_name, data=data)

