from django.urls import path, re_path
from . import views

app_name = 'blog'

urlpatterns = [
    re_path('(?P<article_type>[\w-]+)/(?P<slug>[\w-]+)/$',
            views.ArticleDetailView.as_view(), name='detail'),

    re_path('(?P<article_type>[\w-]+)/$',
            views.ArticleListView.as_view(), name='list'),
]
