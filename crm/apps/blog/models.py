from django.db import models
from django.utils import timezone

from organization.models import Organization
from ckeditor.fields import RichTextField


class Article(models.Model):

    ARTICLES = 'articles'
    NEWS = 'news'

    ARTICLE_TYPE_CHOICES = (
        (ARTICLES, 'Статьи'),
        (NEWS, 'Новости'),
    )

    organization = models.ForeignKey(
        Organization, verbose_name='Организация', on_delete=models.SET_NULL,
        null=True, blank=True)
    article_type = models.CharField(
        max_length=8, verbose_name='Тип', default=ARTICLES,
        choices=ARTICLE_TYPE_CHOICES)
    title = models.CharField(max_length=200, verbose_name='Заголовок')
    slug = models.SlugField()
    content = RichTextField(verbose_name='Контент')
    published = models.DateTimeField(verbose_name='Дата публикации',
                                     default=timezone.now)
    updated = models.DateTimeField(auto_now=True,
                                   verbose_name='Дата изменения')
    views_number = models.PositiveIntegerField(
        verbose_name='Количество просмотров', default=0)

    class Meta:
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'
        ordering = ('-published',)

    def __str__(self):
        return self.title

    @classmethod
    def get_article_type_name(cls, type_: str) -> str:
        return dict((k, v) for k, v in cls.ARTICLE_TYPE_CHOICES).get(type_)

    def get_absolute_url(self):
        return ''
