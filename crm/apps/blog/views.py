from django.views.generic.list import ListView
from django.views.generic.detail import DetailView


from .models import Article


class ArticleDetailView(DetailView):
    model = Article
    template_name = 'blog/detail.html'

    def get_object(self, queryset=None):
        obj = super().get_object(queryset=queryset)
        obj.views_number += 1
        obj.save()
        return obj


class ArticleListView(ListView):
    model = Article
    template_name = 'blog/list.html'

    def get_queryset(self):
        qs = super().get_queryset()
        article_type = self.kwargs['article_type']
        qs = qs.filter(article_type=article_type)
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['article_type_name'] = Article.get_article_type_name(
            self.kwargs['article_type'])
        return context

