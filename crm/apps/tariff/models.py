import random
import string
from decimal import Decimal

from ckeditor.fields import RichTextField
from django.db import models
from django.db.models.functions import Concat
from django.contrib.contenttypes.models import ContentType
from django.core.validators import MinValueValidator, MaxValueValidator
from django.utils.timezone import now, localtime

from core.models import User
from organization.models import Organization
from payment.models import Payment


class Section(models.Model):
    organization = models.ForeignKey(
        Organization, verbose_name='Организация', on_delete=models.CASCADE)
    name = models.CharField('Название раздела', max_length=120)
    sorting_number = models.PositiveIntegerField('Номер по порядку')
    description = RichTextField(verbose_name='Описание')

    class Meta:
        verbose_name = 'Раздел'
        verbose_name_plural = 'Разделы'
        ordering = ['sorting_number']

    def __str__(self):
        return self.name


class Course(models.Model):
    organization = models.ForeignKey(
        Organization, verbose_name='Организация', on_delete=models.CASCADE)
    section = models.ForeignKey(
        Section, verbose_name='Раздел', on_delete=models.SET_NULL, null=True,
        blank=True)
    name = models.CharField('Название курса', max_length=120)
    sorting_number = models.PositiveIntegerField('Номер по порядку')
    description = RichTextField(verbose_name='Описание')
    updated = models.DateTimeField('Обновлен', auto_now=True)

    class Meta:
        verbose_name = 'Курс'
        verbose_name_plural = 'Курсы'
        ordering = ['sorting_number']

    def __str__(self):
        return self.name


class Tariff(models.Model):

    LESSON_DURATION_HOUR = 2

    course = models.ForeignKey(Course, on_delete=models.CASCADE,
                               verbose_name='Курс')
    name = models.CharField('Название', max_length=100)
    is_active = models.BooleanField(blank=True, default=True)
    cost = models.PositiveIntegerField('Стоимость')
    lesson_count = models.PositiveIntegerField('Количество занятий')
    expiration_date = models.PositiveIntegerField('Срок действия в днях')
    updated = models.DateTimeField('Обновлен', auto_now=True)

    class Meta:
        verbose_name = 'Тариф'
        verbose_name_plural = 'Тарифы'

    def __str__(self):
        return f'{self.name} {self.course}'

    def get_hour_num(self):
        return int(self.lesson_count * self.LESSON_DURATION_HOUR)

    def get_hour_cost(self):
        return int(self.cost/self.lesson_count/self.LESSON_DURATION_HOUR)

    def get_season_ticket_name(self):
        return self.__str__()


class SeasonTicketWithLessonsManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().annotate(
            lesson_count=models.Count('studentonlesson')).\
            filter(lesson_count__gte=0)


class SeasonTicket(models.Model):
    CODE_LENGTH = 11
    seller = models.ForeignKey(User, verbose_name='Продавец',
                               on_delete=models.SET_NULL, blank=True, null=True,
                               related_name='seller')
    buyer = models.ForeignKey(User, verbose_name='Покупатель',
                              on_delete=models.SET_NULL, blank=True, null=True,
                              related_name='buyer')
    tariff = models.ForeignKey(
        Tariff, verbose_name='Тариф', on_delete=models.CASCADE)

    expiration_date = models.DateField('Дата окончания', blank=True, null=True)
    code = models.CharField('Код', max_length=CODE_LENGTH, unique=True,
                            null=True)
    student = models.ForeignKey(User, on_delete=models.SET_NULL,
                                null=True,
                                verbose_name='Обучаемый',
                                related_name='student',)
    discount = models.PositiveIntegerField(
        'Скидка', default=0,
        validators=[MinValueValidator(0), MaxValueValidator(100)])
    comment = models.TextField('Коментарий', blank=True, null=True)
    created = models.DateTimeField('Дата покупки', auto_now_add=True, )
    updated = models.DateTimeField('Дата обновления', auto_now=True, )

    class Meta:
        verbose_name = 'Абонемент'
        verbose_name_plural = 'Абонементы'
        ordering = ('-created', )

    def __str__(self):
        return f'{self.student} {self.tariff}'

    @property
    def owner(self):
        return self.student

    @property
    def cost(self):
        return Decimal(self.tariff.cost - self.tariff.cost * self.discount / 100) \
            if self.discount else self.tariff.cost
    cost.fget.short_description = 'Стоимость'

    def generate_code(self):
        rnd_nums = ''.join(
            random.choice(string.digits) for _ in range(self.CODE_LENGTH-3))
        return f'{self.tariff.course.organization.id}-{rnd_nums}'

    def save(self, *args, **kwargs):
        if not self.code:
            self.code = self.generate_code()
        super(SeasonTicket, self).save(*args, **kwargs)

    @property
    def payment_sum(self):
        payments_sum = Payment.objects.filter(
            content_type=ContentType.objects.get_for_model(self),
            object_id=self.id, status=Payment.PAID).aggregate(
            models.Sum('amount')
        )
        return payments_sum['amount__sum'] or 0

    @property
    def debt(self) -> bool:
        return self.cost - self.payment_sum

    debt.fget.short_description = 'Долг'

    @property
    def lesson_balance(self):
        from lesson.models import StudentOnLesson
        now_date = localtime(now())
        end_lesson_count = (
            StudentOnLesson.objects
            .annotate(
                lesson_datetime_end=Concat(
                    'lesson__date', models.Value(' '), 'lesson__time_end',
                    output_field=models.CharField()))
            .filter(season_ticket=self,
                    lesson_datetime_end__lt=str(now_date))).count()
        return self.tariff.lesson_count - end_lesson_count
    lesson_balance.fget.short_description = 'Осталось уроков'


class Confidant(models.Model):
    season_ticket = models.ForeignKey(SeasonTicket, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.user}'

    class Meta:
        verbose_name = 'Довереное лицо'
        verbose_name_plural = 'Довереные лица'
