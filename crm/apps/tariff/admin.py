from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline

from .models import Section, Course, Tariff, SeasonTicket
from lesson.admin import StudentOnLessonInline
from payment.models import Payment


class CourseAdmin(admin.ModelAdmin):
    list_display = ('name', )
    search_fields = ('name', 'description')


class TariffAdmin(admin.ModelAdmin):
    list_display = ('name', 'cost', 'course', 'lesson_count', 'expiration_date')
    list_filter = ('course',)


class PaymentInline(GenericTabularInline):
    model = Payment
    extra = 0
    raw_id_fields = ('user',)
    exclude = ('info', 'acquiring_order_id')


class SeasonTicketAdmin(admin.ModelAdmin):
    list_display = ('id', 'student', 'lesson_balance', 'debt', 'cost', 'tariff',
                    'buyer', 'expiration_date')
    readonly_fields = ('code', 'created', 'debt',)
    search_fields = ('id', 'student__first_name', 'student__last_name',)
    list_filter = ('tariff__name', 'tariff__course__name',
                   'tariff__course__section__name', 'created')
    raw_id_fields = ('student', 'buyer', 'seller')
    inlines = (PaymentInline, StudentOnLessonInline)


admin.site.register(Section)
admin.site.register(SeasonTicket, SeasonTicketAdmin)
admin.site.register(Tariff, TariffAdmin)
admin.site.register(Course, CourseAdmin)
