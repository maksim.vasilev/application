from django.urls import path
from . import views

app_name = 'tariff'

urlpatterns = [
    path('abonement/', views.TariffListView.as_view(),
         name='tariff_list'),
    path('opisanie/', views.CourseListView.as_view(),
         name='course_list'),

    path('abonementy/kupit/<tariff_pk>',
         views.SeasonTicketCreateView.as_view(),
         name='season_ticket_create'),

]
